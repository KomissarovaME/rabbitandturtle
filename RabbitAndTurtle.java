package rabbitAndTurtle;

/*
 *  Данный класс Разминка создан для того чтобы продемонстрировать одновременный запуск нескольких потоков.
 *  При этом и реализацию метода run.
 *
 * @author Komissarova M.E. 16IT18K
 */
public class RabbitAndTurtle {
    public static void main(String[] args) {
        AnimalThread rabbit = new AnimalThread("кролик", 10);
        AnimalThread turtle = new AnimalThread("черепаха", 1);
        rabbit.start();
        turtle.start();

    }
}
